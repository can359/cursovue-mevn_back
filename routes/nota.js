import express from 'express'
import Nota from '../models/nota'
import { verificarAuth } from "../middlewares/auth";

const router = express.Router()

// Agregar una nota
router.post('/nota', verificarAuth, async(req, res) => {
  const body = req.body
  body.usuarioId = req.usuario._id
  try {
    const notaDB = await Nota.create(body)
    res.status(200).json(notaDB)
  } catch (error) {
    return res.status(500).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
})

// Get con parametros
router.get('/nota/:id', async(req, res) => {
  const _id = req.params.id
  try {
    const notaDB = await Nota.findOne({_id})
    res.json(notaDB)
  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
})

// Get con todos los documentos
// router.get('/nota', verificarAuth, async(req, res) => {
//   const usuarioId = req.usuario._id
//   try {
//     const notaDB = await Nota.find({usuarioId})
//     res.json(notaDB)
//   } catch (error) {
//     return res.status(400).json({
//       mensaje: 'Ocurrio un error',
//       error
//     })
//   }
// })

// Get con paginacion
router.get('/nota', verificarAuth, async(req, res) => {
  const usuarioId = req.usuario._id
  const limite = Number(req.query.limite) || 5
  const skip = Number(req.query.skip) || 0
  try {
    const notaDB = await Nota.find({usuarioId}).limit(limite).skip(skip)

    // Contar documentos
    const totalNotas = await Nota.find({usuarioId}).countDocuments()

    res.json({notas: notaDB, totalNotas})
  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
})

// Eliminar nota
router.delete('/nota/:id', async(req, res) => {
  const _id = req.params.id
  try {
    const notaDB = await Nota.findByIdAndDelete({_id})
    res.json(notaDB)
  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
})

// Actualizar nota
router.put('/nota/:id', async(req, res) => {
  const _id = req.params.id
  const body = req.body
  try {
    const notaDB = await Nota.findByIdAndUpdate(
      _id,
      body,
      {new: true}
    )
    res.json(notaDB)
  } catch (error) {
    return res.status(500).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
})

// Exporta la configuracion de express app
module.exports = router
