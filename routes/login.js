import express from 'express'
import User from '../models/user'

const router = express.Router()
// Hash contrasena
const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
const secret = ':a:YXfx$G!/!ziQ!UXupAr)2R(gr)V'

router.post('/', async(req, res) => {
  const body = req.body
  try {
    // Evaluar email
    const userDB = await User.findOne({email: body.email})
    if (!userDB) {
      return res.status(400).json({
        mensaje: 'Email no encontrado'
      })
    }

    // Evaluar pass
    if (!bcrypt.compareSync(body.pass, userDB.pass)) {
      return res.status(400).json({
        mensaje: 'Contraseña incorrecta'
      })
    }

    // Generar token
    const token = jwt.sign({
      data: userDB
    }, secret, { expiresIn: 60 * 60 * 24 * 30 });

    res.json({
      usuario: userDB,
      token: token
    })
  } catch (error) {
    return res.status(500).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
})

module.exports = router
